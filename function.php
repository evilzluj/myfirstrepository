<?php
$one = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$two = array(2, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$three = array(3, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$four = array(4, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$five = array(5, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$six = array(6, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$seven = array(7, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$eight = array(8, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$nine = array(9, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$ten = array(10, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$matrix = array($one, $two, $three, $four, $five, $six, $seven, $eight, $nine, $ten);
	/**
    *   Написать функцию на php, которая принимает 4 параметра:
    *   - двумерный массив элементов
    *   - строковый параметр со значениями 'row', 'column'
    *   - строковый параметр со значениями 'left', 'right', 'top', 'bottom'
    *   - индекс строки или столбца соответственно
    *
    *   Функция должна отобразить относительно указанной оси часть двумерного массива, указанного 3-м параметром.
    *   Например, function f($m, 'row', 'top', 5) - отобразить относительно 5 горизонтального ряда элементы, находящиеся сверху.
	*/
showPartOfArr($matrix, 'column', 'right', 2);

function showPartOfArr($array, $axis, $side, $index){
    $arr = $array;
    define("WRONGARG", "Неверный входящий параметр");
    switch($side){
        case 'left':
            if($axis == 'column'){
                for($i=0; $i < count($arr); $i++)
                {
                    for($q=0; $q < $index; $q++)
                    {
                        echo  " » ".$arr[$i][$q];
                    }
                    echo "<br>";
                }
            }else {
                echo WRONGARG;
            }
            break;
        case 'right':
            if($axis == 'column'){
                for($i=0; $i < count($arr); $i++)
                {
                    for($q=$index; $q < count($arr[$i]); $q++)
                    {
                        echo  " » ".$arr[$i][$q];
                    }
                    echo "<br>";
                }
            }else{
                echo WRONGARG;
            }
            break;
        case 'top':
            if($axis == 'row'){
                for($i=0; $i < $index; $i++)
                {
                    for($q=0; $q < count($arr[$i]); $q++)
                    {
                        echo  " » ".$arr[$i][$q];
                    }
                    echo "<br>";
                }
            }else {
                echo WRONGARG;
            }
            break;
        case 'bottom':
            if($axis == 'row'){
                for($i=$index; $i < count($arr); $i++)
                {
                    for($q=0; $q < count($arr[$i]); $q++)
                    {
                        echo  " » ".$arr[$i][$q];
                    }
                    echo "<br>";
                }
            }else {
                echo WRONGARG;
            }
            break;
    }
}
?>